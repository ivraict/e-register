<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->institution_id = 1;
        $user->group_id = 1;
        $user->name = 'Ivra';
        $user->email = 'asd@mail.com';
        $user->password = bcrypt('asdasd');
        $user->assignRole('student');
        $user->save();
        $user = new User();
        $user->institution_id = 1;
        $user->name = 'dsada';
        $user->email = 'employee@example.com';
        $user->password = bcrypt('asdasd');
        $user->assignRole('admin');
        $user->save();
    }
}

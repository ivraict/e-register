<?php

use App\Lesson;
use Illuminate\Database\Seeder;

class LessonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lesson = new Lesson();
        $lesson->subject_id = 1;
        $lesson->date = '2019-4-26';
        $lesson->start_time = '13:30:00';
        $lesson->end_time = '15:30:00';
        $lesson->save();
    }
}

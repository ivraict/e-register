<?php

use App\Institution;
use Illuminate\Database\Seeder;

class InstitutionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $institution = new Institution();
        $institution->name = 'Test school';
        $institution->save();
    }
}

<div class="col-sm-3">
    <div class="card">
        <div class="card-header">Manage</div>
        <div class="card-body">

            <div class="list-group">
                <a href="{{ url('/admin/') }}" class="list-group-item list-group-item-action">Dashboard</a>
                <a href="{{ url('/admin/groups/') }}" class="list-group-item list-group-item-action">Groups</a>
                <a href="{{ url('/admin/students/') }}" class="list-group-item list-group-item-action">Students</a>
                <a href="{{ url('/admin/lessons/') }}" class="list-group-item list-group-item-action">Lessons</a>
                <a href="{{ url('/admin/subjects/') }}" class="list-group-item list-group-item-action">Subjects</a>
            </div>
        </div>
    </div>
</div>

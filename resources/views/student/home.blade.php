@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Start Time</th>
                                    <th scope="col">End Time</th>
                                    <th scope="col">Presence</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($lessons as $lesson)
                                    <tr>
                                        <td>{{$lesson->subject->name}}</td>
                                        <td>{{date('d M Y', strtotime($lesson->date))}}</td>
                                        <td>{{$lesson->start_time}}</td>
                                        <td>{{$lesson->end_time}}</td>
                                        <td>@if (date('Y-m-d') == $lesson->date && date('H:m:s') >= $lesson->start_time && date('H:m:s') <= $lesson->end_time)
                                                <button type="button" class="btn btn-primary">Present</button>
                                                @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

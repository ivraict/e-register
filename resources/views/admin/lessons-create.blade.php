@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            @component ('components.navigation')
            @endcomponent
            <div class="col-sm-8">
                <div class="card">
                    <div class="card-header">Create lesson</div>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="card-body">
                        <form method="POST" action="{{ url('admin/lessons/store') }}">
                            @csrf
                            <div class="form-group">
                                <label for="subjectSelect">Subject</label>
                                <select name="subject_id" class="form-control" id="subjectSelect">
                                    @foreach ($subjects as $subject)
                                    <option value="{{$subject->id}}">{{$subject->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="groupSelect">Group (can select multiple using ctrl + click)</label>
                                <select multiple="multiple" name="groups[]" class="form-control" id="groupSelect">
                                    @foreach ($groups as $group)
                                    <option value="{{$group->id}}">{{$group->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="date">Date</label>
                                <input name="date" type="date" class="form-control" id="date">
                            </div>
                            <div class="row">
                                <div class="col">
                                    <label for="startTime">Start Time</label>
                                    <input name="start_time" type="text" class="form-control" id="startTime">
                                </div>
                                <div class="col">
                                    <label for="endTime">End Time</label>
                                    <input name="end_time" type="text" class="form-control" id="startTime">
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

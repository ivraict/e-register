@extends('layouts.app')

@section('content')


    <div class="container">
        <div class="row">
            @component ('components.navigation')
            @endcomponent

            <div class="col-sm-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        Institution: {{ auth()->user()->institution->name }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

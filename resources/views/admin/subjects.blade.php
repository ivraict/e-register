@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            @component ('components.navigation')
            @endcomponent

            <div class="col-sm-8">
                <div class="card">
                    <div class="card-header">Subjects</div>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="card-body">
                        <a class="btn btn-primary" href="{{ url('/admin/subjects/create') }}" role="button">Create</a>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">Name</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($subjects as $subject)
                                    <tr>
                                        <td>
                                            {{$subject->name}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $subjects->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

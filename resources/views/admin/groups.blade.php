@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            @component ('components.navigation')
            @endcomponent

            <div class="col-sm-8">
                <div class="card">
                    <div class="card-header">Groups</div>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="card-body">
                        <a class="btn btn-primary" href="{{ url('/admin/groups/create') }}" role="button">Create</a>
                        <div class="list-group">
                            @foreach ($groups as $group)
                                <a href="{{ url('admin/groups/show/' . $group->id) }}" class="list-group-item list-group-item-action">{{$group->name}}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            @component ('components.navigation')
            @endcomponent

            <div class="col-sm-8">
                <div class="card">
                    <div class="card-header">Lessons</div>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="card-body">
                        <a class="btn btn-primary" href="{{ url('/admin/lessons/create') }}" role="button">Create</a>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Group(s)</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Start Time</th>
                                    <th scope="col">End Time</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($lessons as $lesson)
                                    <tr>
                                        <td>{{$lesson->subject->name}}</td>
                                        <td>@foreach ($lesson->groups as $group)
                                                {{$group->name}}
                                            @endforeach
                                        </td>
                                        <td>{{date('d M Y', strtotime($lesson->date))}}</td>
                                        <td>{{$lesson->start_time}}</td>
                                        <td>{{$lesson->end_time}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $lessons->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            @component ('components.navigation')
            @endcomponent
            <div class="col-sm-8">
                <div class="card">
                    <div class="card-header">Create student</div>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="card-body">
                        <form method="POST" action="{{ url('admin/students/store') }}">
                            @csrf
                            <div class="form-group">
                                <label>Name</label>
                                <input name="name" type="text" class="form-control" placeholder="Enter name">
                            </div>
                            <div class="form-group">
                                <label for="inputEmail">Email address</label>
                                <input name="email" type="email" class="form-control" id="inputEmail" aria-describedby="emailHelp" placeholder="Enter email">
                            </div>
                            <div class="form-group">
                                <label for="groupSelect">Group</label>
                                <select name="group_id" class="form-control" id="groupSelect">
                                    @foreach ($groups as $group)
                                        <option value="{{$group->id}}">{{$group->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

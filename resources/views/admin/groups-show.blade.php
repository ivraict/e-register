@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            @component ('components.navigation')
            @endcomponent

            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Group {{$group->name}}</div>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="card-body">

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">Name</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                @foreach ($users as $user)
                                    <td>
                                        {{$user->name}}
                                    </td>
                                @endforeach
                            </tr>
                            </tbody>
                        </table>
                        {{ $users->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

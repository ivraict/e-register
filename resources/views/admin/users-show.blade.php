@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            @component ('components.navigation')
            @endcomponent

            <div class="col-sm-8">
                <div class="card">
                    <div class="card-header">{{$user->name}}</div>

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th scope="col">Subject</th>
                                <th scope="col">Date</th>
                                <th scope="col">Start Time</th>
                                <th scope="col">End Time</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($user->group->lessons as $lesson)
                                <tr>
                                    <td>{{$lesson->subject->name}}</td>
                                    <td>{{$lesson->date}}</td>
                                    <td>{{$lesson->start_time}}</td>
                                    <td>{{$lesson->end_time}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (auth()->check()){
        return redirect()->back();
    }
    return view('welcome');
});

Auth::routes();

Route::middleware(['student'])->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
});

Route::middleware(['admin'])->group(function () {
    Route::prefix('admin')->group(function () {
        Route::get('/', 'AdminController@index');

        Route::prefix('groups')->group(function () {
            Route::get('/', 'GroupController@index');
            Route::get('/create', 'GroupController@create');
            Route::post('/store', 'GroupController@store');
            Route::get('/show/{id}', 'GroupController@show');
        });

        Route::prefix('students')->group(function () {
            Route::get('/', 'UserController@index');
            Route::get('/create', 'UserController@create');
            Route::post('/store', 'UserController@store');
            Route::get('/show/{id}', 'UserController@show');
        });

        Route::prefix('lessons')->group(function () {
            Route::get('/', 'LessonController@index');
            Route::get('/create', 'LessonController@create');
            Route::post('/store', 'LessonController@store');
        });

        Route::prefix('subjects')->group(function () {
            Route::get('/', 'SubjectController@index');
            Route::get('/create', 'SubjectController@create');
            Route::post('/store', 'SubjectController@store');
        });
    });
});

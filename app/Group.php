<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    public function lessons(){
        return $this->belongsToMany('App\Lesson');
    }

    public function user(){
        return $this->hasMany('App\User');
    }
}

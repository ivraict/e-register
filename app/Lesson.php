<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [
    ];

    public function groups(){
        return $this->belongsToMany('App\Group');
    }

    public function subject(){
        return $this->belongsTo('App\Subject');
    }
}
